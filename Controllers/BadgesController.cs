﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeightScanDBAPI.Models;

namespace WeightScanDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BadgesController : ControllerBase
    {
        private readonly WeightScanDBContext _context;

        public BadgesController(WeightScanDBContext context)
        {
            _context = context;
        }

        // GET: api/Badges
        [HttpGet]
        public IEnumerable<Badge> GetBadge()
        {
            return _context.Badge;
        }

        // GET: api/Badges/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBadge([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var badge = await _context.Badge.FindAsync(id);

            if (badge == null)
            {
                return NotFound();
            }

            return Ok(badge);
        }

        // PUT: api/Badges/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBadge([FromRoute] int id, [FromBody] Badge badge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != badge.Id)
            {
                return BadRequest();
            }

            _context.Entry(badge).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BadgeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Badges
        [HttpPost]
        public async Task<IActionResult> PostBadge([FromBody] Badge badge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Badge.Add(badge);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBadge", new { id = badge.Id }, badge);
        }

        // DELETE: api/Badges/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBadge([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var badge = await _context.Badge.FindAsync(id);
            if (badge == null)
            {
                return NotFound();
            }

            _context.Badge.Remove(badge);
            await _context.SaveChangesAsync();

            return Ok(badge);
        }

        private bool BadgeExists(int id)
        {
            return _context.Badge.Any(e => e.Id == id);
        }
    }
}