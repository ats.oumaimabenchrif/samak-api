﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeightScanDBAPI.Models;

namespace WeightScanDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PesagesController : ControllerBase
    {
        private readonly WeightScanDBContext _context;

        public PesagesController(WeightScanDBContext context)
        {
            _context = context;
        }

        // GET: api/Pesages
       /* [HttpGet]
        public IEnumerable<Pesage> GetPesage()
        {
            return _context.Pesage;
        }
        */
        // GET: api/Pesages/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPesage([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pesage = await _context.Pesage.FindAsync(id);

            if (pesage == null)
            {
                return NotFound();
            }

            return Ok(pesage);
        }

        // PUT: api/Pesages/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPesage([FromRoute] int id, [FromBody] Pesage pesage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pesage.Id)
            {
                return BadRequest();
            }

            _context.Entry(pesage).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PesageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pesages
        [HttpPost]
        public async Task<IActionResult> PostPesage([FromBody] Pesage pesage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Pesage.Add(pesage);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPesage", new { id = pesage.Id }, pesage);
        }

        // DELETE: api/Pesages/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePesage([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pesage = await _context.Pesage.FindAsync(id);
            if (pesage == null)
            {
                return NotFound();
            }

            _context.Pesage.Remove(pesage);
            await _context.SaveChangesAsync();

            return Ok(pesage);
        }

        private bool PesageExists(int id)
        {
            return _context.Pesage.Any(e => e.Id == id);
        }
        [HttpGet]
        public JsonResult GetPesage()
        {
            var pesages = _context.Pesage.Select(s => new {
                s.Id,
                s.IdPersonne,
                s.Poid,
                s.DatePesage,
                personne = s.IdPersonneNavigation.Nom + " " + s.IdPersonneNavigation.Prenom,
                s.IdPersonneNavigation.Matricule
            }).ToList();
            return new JsonResult(pesages);
        }
        [HttpGet("today")]
        public IEnumerable<Pesage> getPesagesToday()
        {
            return _context.Pesage.Include(p => p.IdPersonneNavigation).Where(p => p.DatePesage.Value.Date == DateTime.Now.Date).ToList();
        }

        [HttpGet("matricule/{mat}/du/{du}/au/{au}")]
        public IEnumerable<Pesage> GetPesagePeriodPersonne([FromRoute] string mat, [FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            return _context.Pesage.Where(d => d.IdPersonneNavigation.Matricule == mat && d.DatePesage.Value.Date >= du.Date && d.DatePesage.Value.Date <= au.Date)
                .Include(p => p.IdPersonneNavigation).ToList();

        }
        [HttpGet("du/{du}/au/{au}")]
        public IEnumerable<Pesage> GetPesagePeriod([FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            return _context.Pesage.Where(d => d.DatePesage.Value.Date >= du.Date && d.DatePesage.Value.Date <= au.Date)
                .Include(p => p.IdPersonneNavigation).ToList();

        }

        [HttpGet("get")]
        public IQueryable GetPesageAndTotalPesage()
        {

            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date == DateTime.Now.Date
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                             Total = pp.Sum(s => s.Poid),
                             prixTotal = 0
                         };
            return result;
        }
        [HttpGet("rechercher/{mat}")]
        public IQueryable GetRecherchePesageAndTotalPesage([FromRoute] string mat)
        {

            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date == DateTime.Now.Date && p.IdPersonneNavigation.Matricule == mat
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                             Total = pp.Sum(s => s.Poid),
                             prixTotal = 0
                         };
            return result;
        }
        [HttpGet("rechercherdeuxdate/{db}/au/{df}")]
        public IQueryable GetRecherchePesageAndTotalPesagedate([FromRoute] DateTime db, [FromRoute] DateTime df)
        {

            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value >= db && p.DatePesage.Value <= df
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                             Total = pp.Sum(s => s.Poid),
                             prixTotal = 0
                         };
            return result;
        }


        [HttpGet("mat/{mat}/du/{db}/au/{df}")]
        public IQueryable GetRecherchePesageAndTotalPesagedateall([FromRoute] string mat, [FromRoute] DateTime db, [FromRoute] DateTime df)
        {

            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date >= db && p.DatePesage.Value.Date <= df && p.IdPersonneNavigation.Matricule == mat
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                             Total = pp.Sum(s => s.Poid),
                             prixTotal = 0
                         };
            return result;
        }
        [HttpGet("getPrixTotal/{prix}/{du}/{au}")]
        public IQueryable getPrixTotal(int prix, DateTime du, DateTime au)
        {
            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date >= du && p.DatePesage.Value.Date <= au
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                             Total = pp.Sum(s => s.Poid),
                             PrixTotal = prix * pp.Sum(s => s.Poid)
                         };
            return result;
        }
        [HttpGet("total/{du}/au/{au}/{mat}")]
        public IQueryable GetPesagePeriodTotal([FromRoute] DateTime du, [FromRoute] DateTime au, [FromRoute] string mat)
        {
            //return await _context.Pesage.Where(d => d.DatePesage.Value.Date >= du.Date && d.DatePesage.Value.Date <= au.Date).Include(l => l.IdPersonneNavigation).ToListAsync();
            if (mat.Equals(null))
            {
                var rep = from p in _context.Pesage
                             join ps in _context.Personne on p.IdPersonne equals ps.Id
                             where p.DatePesage.Value >= du && p.DatePesage.Value <= au
                             group p by new
                             {
                                 p.IdPersonne,
                                 p.IdPersonneNavigation.Matricule,
                                 p.IdPersonneNavigation.Nom,
                                 p.IdPersonneNavigation.Id,
                                 p.IdPersonneNavigation.Prenom
                             } into pp
                             select new
                             {
                                 Matricule = pp.Key.Matricule,
                                 personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                                 Total = pp.Sum(s => s.Poid),
                                 PrixTotal = 0
                             };
                return rep;

            }
                var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value >= du && p.DatePesage.Value <= au
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                             Total = pp.Sum(s => s.Poid),
                             PrixTotal = 0
                         };
            return result;
        }
        [HttpGet("total/{mat}/du/{du}/au/{au}")]
        public IQueryable GetPesagePeriodPersonneTotal([FromRoute] string mat, [FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            //return _context.Pesage.Where(d => d.IdPersonneNavigation.Matricule == mat && d.DatePesage.Value.Date >= du.Date && d.DatePesage.Value.Date <= au.Date);

            if (!mat.Equals(null)) {
                var rep = from p in _context.Pesage
                             join ps in _context.Personne on p.IdPersonne equals ps.Id
                             where p.IdPersonneNavigation.Matricule == mat
                             && p.DatePesage.Value >= du && p.DatePesage <= au
                             group p by new
                             {
                                 p.IdPersonne,
                                 p.IdPersonneNavigation.Matricule,
                                 p.IdPersonneNavigation.Nom,
                                 p.IdPersonneNavigation.Id,
                                 p.IdPersonneNavigation.Prenom
                             } into pp
                             select new
                             {
                                 Matricule = pp.Key.Matricule,
                                 personne = pp.Key.Prenom + "  " + pp.Key.Nom,
                                 Total = pp.Sum(s => s.Poid),
                                 PrixTotal = 0
                             };
                return rep;
            }
            return null;


        }
    }
}