﻿using System;
using System.Collections.Generic;

namespace WeightScanDBAPI.Models
{
    public partial class Login
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public string Logout { get; set; }
        public DateTime DateLogin { get; set; }
        public DateTime? DateLogout { get; set; }

        public User IdUserNavigation { get; set; }
    }
}
