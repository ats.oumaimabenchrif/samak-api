﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WeightScanDBAPI.Models
{
    public partial class WeightScanDBContext : DbContext
    {
        public WeightScanDBContext()
        {
        }

        public WeightScanDBContext(DbContextOptions<WeightScanDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Badge> Badge { get; set; }
        public virtual DbSet<Login> Login { get; set; }
        public virtual DbSet<Personne> Personne { get; set; }
        public virtual DbSet<Pesage> Pesage { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
           //     optionsBuilder.UseSqlServer("server=.;database=WeightScanDB;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Badge>(entity =>
            {
                entity.ToTable("badge");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NumeroBadge).HasMaxLength(100);

                entity.Property(e => e.NumeroSerie).HasMaxLength(100);
            });

            modelBuilder.Entity<Login>(entity =>
            {
                entity.ToTable("login");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateLogin)
                    .HasColumnName("dateLogin")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateLogout)
                    .HasColumnName("dateLogout")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.Logout)
                    .HasColumnName("logout")
                    .HasMaxLength(5);

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Login)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__login__idUser__1920BF5C");
            });

            modelBuilder.Entity<Personne>(entity =>
            {
                entity.ToTable("personne");

                entity.HasIndex(e => e.Matricule)
                    .HasName("UQ__personne__30962D11C7DD0BF2")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Adresse)
                    .HasColumnName("adresse")
                    .HasMaxLength(100);

                entity.Property(e => e.Cin)
                    .HasColumnName("cin")
                    .HasMaxLength(100);

                entity.Property(e => e.Cnss)
                    .HasColumnName("cnss")
                    .HasMaxLength(100);

                entity.Property(e => e.DateArret)
                    .HasColumnName("dateArret")
                    .HasColumnType("date");

                entity.Property(e => e.DateEmbauche)
                    .HasColumnName("dateEmbauche")
                    .HasColumnType("date");

                entity.Property(e => e.DateNaissance)
                    .HasColumnName("dateNaissance")
                    .HasColumnType("date");

                entity.Property(e => e.Idbadge).HasColumnName("IDBadge");

                entity.Property(e => e.Matricule)
                    .IsRequired()
                    .HasColumnName("matricule")
                    .HasMaxLength(200);

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasMaxLength(100);

                entity.Property(e => e.NombreEnfant).HasColumnName("nombreEnfant");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(14);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(100);

                entity.Property(e => e.Situation)
                    .HasColumnName("situation")
                    .HasMaxLength(100);

                entity.Property(e => e.Ville)
                    .HasColumnName("ville")
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdbadgeNavigation)
                    .WithMany(p => p.Personne)
                    .HasForeignKey(d => d.Idbadge)
                    .HasConstraintName("FK__personne__IDBadg__49C3F6B7");
            });

            modelBuilder.Entity<Pesage>(entity =>
            {
                entity.ToTable("pesage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DatePesage)
                    .HasColumnName("datePesage")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPersonne).HasColumnName("idPersonne");

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.Poid).HasColumnName("poid");

                entity.HasOne(d => d.IdPersonneNavigation)
                    .WithMany(p => p.Pesage)
                    .HasForeignKey(d => d.IdPersonne)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__pesage__idPerson__164452B1");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Pesage)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK__pesage__idUser__34C8D9D1");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Nom)
                    .HasColumnName("nom")
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(100);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(14);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(100);

                entity.Property(e => e.Role)
                    .HasColumnName("role")
                    .HasMaxLength(100);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(100);
            });
        }
    }
}
