﻿using System;
using System.Collections.Generic;

namespace WeightScanDBAPI.Models
{
    public partial class Pesage
    {
        public int Id { get; set; }
        public int IdPersonne { get; set; }
        public double Poid { get; set; }
        public DateTime? DatePesage { get; set; }
        public int? IdUser { get; set; }

        public Personne IdPersonneNavigation { get; set; }
        public User IdUserNavigation { get; set; }
    }
}
