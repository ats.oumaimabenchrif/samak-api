﻿using System;
using System.Collections.Generic;

namespace WeightScanDBAPI.Models
{
    public partial class Badge
    {
        public Badge()
        {
            Personne = new HashSet<Personne>();
        }

        public int Id { get; set; }
        public string NumeroBadge { get; set; }
        public string NumeroSerie { get; set; }

        public ICollection<Personne> Personne { get; set; }
    }
}
